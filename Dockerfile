# twitter2osc Dockerfile
#
# Author: Thesix
#
# Version 0.0.1

FROM ubuntu
LABEL maintainer="thesix, https://thesix.mur.at/"

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    python3-tweepy \
    python3-langdetect \
    python3-osc-lib \
    python3-pip \
    python3-setuptools \
    python3-wheel \
    python3-matplotlib \
    python3-tk \
    locales \
    tzdata \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8 \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV LANG en_US.utf8
RUN pip3 install gensim
RUN pip3 install wordcloud

ADD /app /app

WORKDIR /app
ENTRYPOINT ["./t2osc.py"]
