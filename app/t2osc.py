#!/usr/bin/env python3

from tweepy import Stream
from tweepy import OAuthHandler
from tweepy.streaming import StreamListener
import json
import tweepy
import subprocess
import sys
import argparse
import re
from langdetect import detect

# installed via pip3
from gensim import corpora
from gensim import models
from wordcloud import WordCloud

# local module OSC3.py
from OSC3 import *

class OSCSender:
    def __init__(self, serverport=None):
        self.osc = OSCClient()
        if serverport:
            self.osc.connect(serverport)
    def connect(self, serverport):
        if self.osc.address():
            print("ignoring old address")
        self.osc.connect(serverport)
    def send(self, msg, data=None):
        if type(msg) in [OSCMessage, OSCBundle]:
            return self.osc.send(msg)
        return self.osc.send(OSCMessage(msg, data))


dictionary = corpora.Dictionary.load("dictionary.gensim")
ldamodel = models.ldamodel.LdaModel.load("model5.gensim")

class listener(StreamListener):

    def on_status(self, status):
        listtopics = []
        listweights = []
        textowrite = []

        try:
            t = status._json["extended_tweet"]["full_text"]
        except Exception as e:
            t = status.text

        try:
            lang = detect(t)
            if lang != "en":
                print('info:  skipping none English tweet.  Lang={}\n'.format(lang))
                return True
        except:
            print('info:  skipping empty tweet\n')
            return True

        print("###################\n{}\n###################".format(t))
        td = dictionary.doc2bow(re.sub(r'\W+', ' ', (t or '').lower()).split())

        for topic in ldamodel.get_document_topics(td, minimum_probability=0.016, minimum_phi_value=None, per_word_topics=True)[0]:
            if topic[0] in [33, 34, 38, 46, 56, 55]:
                continue
            listtopics.append(topic[0])
            listweights.append(float(topic[1]))
            print('\n{} {}\n'.format(topic[0], topic[1]))

            for w in ldamodel.get_topic_terms(topic[0])[0:5]:
                print('{} {}'.format(dictionary[w[0]], w[1]))
                textowrite.append(dictionary[w[0]].strip())

        # why?
        if len(textowrite) < 3:
            textowrite = []

        tmplist = sorted(zip(listweights, listtopics), reverse=True)
        osc.send("/topics", [_[1] for _ in tmplist])
        osc.send("/weights", [_[0] for _ in tmplist])
        osc.send("/topicwords", textowrite)
        print('\n{}\n\n'.format(textowrite))

        # gnerate a wordcloud image
        if imagepath and len(textowrite) > 0:
            text = ''
            for word in textowrite:
                text += '{} '.format(word)
            try:
                wordcloud = WordCloud(width=800, height=400).generate(text)
            except (Exception) as e:
                print('exception creating wordcloud: ', e)
                return True
            try:
                image = wordcloud.to_image()
            except (Exception) as e:
                print('exception creating wordcloud image: ', e)
            image.save(imagepath)

        return True

    def on_error(self, status):
        print(status)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = 'Twitter to OSC')
    parser.add_argument('--host', type=str, default='127.0.0.1', dest='osc_host', help='Host IP')
    parser.add_argument('--port', type=int, default=9999, dest='osc_port', help='UDP port')
    parser.add_argument('--api-key', type=str, default='', dest='api_key', help='Twitter API key', required=True)
    parser.add_argument('--api-secret-key', type=str, default='', dest='api_secret_key', help='Twitter API secret key', required=True)
    parser.add_argument('--api-access-token', type=str, default='', dest='api_access_token', help='Twitter API access token', required=True)
    parser.add_argument('--api-access-token-secret', type=str, default='', dest='api_access_token_secret', help='Twitter API access token secret', required=True)
    parser.add_argument('--image-path', type=str, default='', dest='imagepath', help='Path to a file for a wordcloud')
    args = parser.parse_args()

    global osc
    osc = OSCSender((args.osc_host, args.osc_port))

    global imagepath
    imagepath = None
    if args.imagepath:
        imagepath = args.imagepath

    auth = OAuthHandler(args.api_key, args.api_secret_key)
    auth.set_access_token(args.api_access_token, args.api_access_token_secret)

    try:
        twitterStream = Stream(auth, listener())
        twitterStream.filter(track=["#blockchain"])
    except KeyboardInterrupt:
        print('exiting')
    except Exception as e:
        print('terminating on error: ', e)
